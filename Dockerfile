
# Base alpine images with node.js
FROM docker.io/library/openjdk:17-alpine

# MAINTAINER Your Name Davin Ryan davin_ryan@bnz.co.nz

ENV BUILDER_VERSION 1.0

LABEL io.k8s.description="Platform for building java spring-boot applications" \
      io.k8s.display-name="builder java-maven" \
      io.openshift.expose-services="8080:http,8081:management-http" \
      io.openshift.tags="builder,java,maven" \
      io.openshift.s2i.scripts-url=image:///usr/local/s2i \
      MAINTAINER="Zhan Yi <zyzy257@gmail.com>"

ENV MAVEN_VERSION=3.6.3 \
    HOME=/opt/app-root \
    PATH=/opt/app-root/src/bin:/opt/app-root/bin:/opt/maven/bin/:$PATH \
    BASH_ENV=/opt/app-root/etc/scl_enable \
    ENV=/opt/app-root/etc/scl_enable \
    PROMPT_COMMAND=". /opt/app-root/etc/scl_enable"


# Copy extra files to the image.
COPY ./s2i/root/ /
COPY ./contrib/settings.xml $HOME/.m2/


# Drop the root user and make the content of /opt/app-root owned by user 1001
#RUN useradd -u 1001 -r -g 0 -d ${HOME} -s /sbin/nologin -c "Default Application User" default
RUN apk update && apk upgrade && apk add --no-cache bash shadow curl lsof tar zip net-tools tzdata && rm -rf /var/cache/apk/* && mkdir -p /opt/app-root/  /opt/openshift \
        &&	adduser -u 1001 -g 0 -D -h ${HOME} default \
	&& chown -R 1001:0 /opt/app-root \
	&& chown -R 1001:0 /opt/openshift \
        && curl -Lsk -0 https://archive.apache.org/dist/maven/maven-3/${MAVEN_VERSION}/binaries/apache-maven-${MAVEN_VERSION}-bin.tar.gz | tar -zx -C /usr/local \
	&& mv /usr/local/apache-maven-${MAVEN_VERSION} /usr/local/maven \
        && ln -sf /usr/local/maven/bin/mvn /usr/local/bin/mvn \
        && mkdir -p ${HOME}/.m2 && chmod -R a+rwX ${HOME}/.m2 \ 
        && ln -s /usr/share/zoneinfo/Asia/Shanghai /etc/localtime


# This default user is created in the alpine image
USER 1001

# Set the default port for applications built using this image
EXPOSE 8080
EXPOSE 8081

# Directory with the sources is set as the working directory so all STI scripts
# can execute relative to this path.
WORKDIR ${HOME}

# Set the default CMD for the image
CMD ["usage"]
