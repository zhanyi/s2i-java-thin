IMAGE_NAME = zhanyi/s2i-java-thin

build:
	docker build -t $(IMAGE_NAME) . -f Dockerfile
	docker build -t $(IMAGE_NAME):jdk8 . -f Dockerfile.jdk8

.PHONY: test
test:
	docker build -t $(IMAGE_NAME)-candidate .
	IMAGE_NAME=$(IMAGE_NAME)-candidate BUILDER=maven test/run
